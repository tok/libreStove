# IMPORTANT

**THIS IS ONLY A USELESS SHELL OF WHAT IT USED TO BE**

This repository contained a generator script for a foldable camping stove together with 20 lines of instructions how to construct it yourself. For those who could not manage to run the generator script, the 'release' section also contained DXF files.
A company who sells this stoves commercial was not happy at all about this information being public. Following their request and threats about legal consequences the crucial files where completely removed from this repository.

I hope it is still ok to show pictures of the stove. They can be found everywhere on the internet anway.

RIP LibreStove

# LibreStove 🔥

**DISCLAIMER** This is an independent and non-commercial project. Any such stoves sold anywhere are not under control of the owner of this repository.

TL;DR: DXF files can be downloaded in the section below.

LibreStove is a foldable 'hobo' stove. It is made from metal sheets that are connected with dirt-resistants joints and hence can be compactly and easily folded and unfolded. It is perfect to take on trips where wood is available and no fuel needs to be carried arround.

Actually, LibreStove is not just a static design but a design generator. It is a program that generates the DXF files and is parametrizable. It is therefore easy to generate a stove with slightly different dimensions, other sizes of the joints etc. This makes it easily portable to other machines.

The generator scripts are written in Python and output DXF files suitable for laser cutters, plasma cutters or water jet cutters.

## Assembly instructions

**CENSORED**

## Licence 📜

The generator script as well as the generated designs are licensed under the **CERN OHL-S v2** (Open Hardware License).

## Download DXF files ⭳

DXF files of some versions can be downloaded directly:

**CENSORED**

## Generate a DXF file ⚙

**DOES NOT WORK ANYMORE - THE SCRIPT WAS REMOVED**

The main way to get the DXF files is to run the program and generate them.

On Linux:
Install dependencies:
```bash
sudo apt install python3 git

# Install python-venv (this is for Debian).
sudo apt install python-venv

# Create a virtual environment.
python3 -m venv hobo-env
# Activate the virtual environment.
source hobo-env/bin/activate
# Install KLayout.
pip install klayout
```

Then clone the repository and run the hobo script.
```bash
# Clone this repository
git clone https://codeberg.org/tok/libreStove

# Generate the layout.
python3 hobo.py
```

## Gallery

![DXF layout for plasma cutting (censored).](./illustrations/00_generated_dxf_censored.jpg "Generated DXF.")
![Plasma cutting the parts.](./illustrations/01_plasma_cut.jpg "Plasma cutting the parts.")
![Freshly plasma-cut parts of a libreStove.](./illustrations/02_cut_result.jpg "Freshly cut parts.")
![Assembled libreStove.](./illustrations/03_assembled.jpg "Assembled stove")
![Burn-in of a libreStove.](./illustrations/04_burn-in.jpg "Burn-in.")
![Cooking soup on a libreStove.](./illustrations/05_cook.jpg "Cooking soup.")


"""
Copyright Thomas Kramer, 2020
Licensed under CERN-OHL-S version 2
https://codeberg.org/tok/libreStove
"""

"""
This script generates a test structure which can be used to see how well your CNC cutter works.
It has nothing to do with the stove.
"""

import klayout.db as db
import logging
import math
import numpy as np
import itertools

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(levelname)6s: %(message)s', level=logging.DEBUG)

logger.info("Generating test structures for the CNC cutter!")

# Define units.
um = 1
mm = 1000*um

width = 100*mm
height = 20*mm

# Thickness of metal sheets.
metal_thickness = 1*mm

default_corner_radius = 5*mm

# Joint dimensions.
joint_width = 10*mm
joint_length = 5*mm
joint_strength = 2*mm
joint_radius = 2*mm
joint_asymmetry = 1*mm #0.5*mm
joint_extension = 1*mm
joint_drill_radius = 0.5*mm


def ngon(n: int, radius: int, x: int = 0, y: int = 0, rotation: float = 0) -> db.Polygon:
    """
    Generate a n-gon.
    """
    return db.Polygon(
        [db.Point(radius*math.sin(i/n*2*math.pi + rotation) + x, radius*math.cos(i/n*2*math.pi + rotation) + y) for i in range(0, n)]
    )

def polygon(coords) -> db.Polygon:
    """
    Create a polyogn from an iterable over (x,y) tuples.
    """
    return db.Polygon(
        [db.Point(*c) for c in coords]
    )

def round_corners(region: db.Region, radius: int) -> db.Region:
    region.size(-radius)
    enlarged = region.minkowsky_sum(ngon(16, radius))
    
    return enlarged
    
def smoothen_corners(r: db.Region, radius: int) -> db.Region:
    """
    Do postprocessing depending on the fabrication technology.
    """
    logger.debug(f"Smoothen corners: radius = {radius}.")
    r = round_corners(r, radius=radius)
    bbox = db.Region()
    bbox += r.bbox().enlarged(4*radius, 4*radius)
    
    negative = db.Region()
    negative += bbox
    negative -= r
    negative = round_corners(negative, radius=radius)
    positive = round_corners(bbox, radius=radius) - negative
    
    return positive

def draw_joint(r: db.Region, trans: db.Trans,
        mirror: bool = False,
        draw_full_joint: bool = True):
    #logger.debug("Draw joint.")
    if mirror:
        a = -joint_asymmetry
    else:
        a = joint_asymmetry
        
    cutout = db.Box(db.Point(-joint_width//2, -joint_length), db.Point(joint_width//2, joint_radius*2 + 1*mm))
    joint_holder = db.Box(db.Point(-joint_strength//2+a, -joint_length), db.Point(joint_strength//2+a, joint_extension))
    
    circle_x = a - math.copysign(joint_radius - joint_strength//2, a)
    circle_y = joint_extension
    joint_circle = ngon(16, joint_radius, y=circle_y, x=circle_x)
    joint_drill = ngon(16, joint_drill_radius, y=circle_y, x=circle_x)
    
    r -= trans.trans(cutout)
    if draw_full_joint:
        r += trans.trans(joint_holder)
        r += trans.trans(joint_circle)
        if joint_drill_radius > 0:
            r -= trans.trans(joint_drill)

def draw_v_notch(width: int, length: int, v_size: int = 0, v_length: int = 0) -> db.Region:
    """
    Draw a notch with a V opening.
    """
    logger.debug("Draw v-notch.")
    
    notch = polygon([
        (-(width/2+v_size), 0),
        (width/2+v_size, 0),
        (width/2, -v_length),
        (width/2, -length),
        (-width/2, -length),
        (-width/2, -v_length),
    ])
    
    r = db.Region()
    r += notch
    return r



def create_lattice_holes(obj: db.Region, 
    hole_shape: db.Polygon = ngon(16, 4*mm), 
    hole_pitch: int = 16*mm,
    min_distance_to_boundary: int = 5*mm,
    lattice_vector_a: db.DVector = db.DVector(0, 1),
    lattice_vector_b: db.DVector = db.DVector(math.cos(math.pi/6), math.sin(math.pi/6)),
    ) -> db.Region:
    """
    Fill the region `obj` with holes. The holes are arranged on a lattice
    and have a minimal distance to the boundaries of the region.
    """
    
    logger.debug("Create lattice holes.")
    
    # Draw mesh.
    hole = hole_shape
    
    # Lattice vectors.
    a = lattice_vector_a * hole_pitch
    b = lattice_vector_b * hole_pitch
    
    n_grid = int(max(size_x / b.x, size_y / a.y) + 1)
    
    # Compute potential positions of the wholes (on the lattice).
    positions = [
        a*i + b*j
        for i, j in itertools.product(range(-n_grid, n_grid), range(-n_grid, n_grid))
    ]
    
    holes = db.Region()
    for p in positions:
        trans = db.Trans(int(p.x), int(p.y))
        _hole = trans.trans(hole)
        # Make hole only if it is fully contained in the plate.
        hole_sized = _hole.sized(min_distance_to_boundary)
        intersection = obj & hole_sized
        if intersection.area() == hole_sized.area():
            holes += _hole
    
    return holes


    


layout = db.Layout()
layout.dbu = 1
l_cut = layout.layer(0, 0, "cut")
cell_front = layout.create_cell("front")

s_front = cell_front.shapes(l_cut)

# Draw test structures.
front = db.Region()

box = db.Box(db.Point(0, 0), db.Point(width, height))
front += box

# Notch sweep.
notch_width_start = 0.5*mm
notch_width_end = 4*mm
notch_width_step = 0.25*mm

x_pos = 5*mm
for w in np.arange(notch_width_start, notch_width_end, step=notch_width_step):
    
    notch = draw_v_notch(width=w, length=4*mm)
    notch = notch.transformed(db.Trans(x_pos, height))
    front -= notch
    x_pos += w + 5*mm

# Cantilever sweep.
notch_width_start = 0.5*mm
notch_width_end = 4*mm
notch_width_step = 0.25*mm

x_pos = 5*mm
for w in np.arange(notch_width_start, notch_width_end, step=notch_width_step):
    
    notch = draw_v_notch(width=w, length=4*mm)
    notch = notch.transformed(db.Trans(x_pos, 0))
    front += notch
    x_pos += w + 5*mm
    
front.merge()

# Circle sweep.
d_start = 0.5*mm
d_end = 4*mm
d_step = 0.25*mm

x_pos = 5*mm + d_end/2
for d in np.arange(d_start, d_end, step=d_step):
    r = d/2
    circle = ngon(16, radius=r)
    circle = circle.transformed(db.Trans(x_pos, 5*mm))
    front -= circle
    x_pos += d + 4*mm
    
front.merge()

s_front.insert(front)


output_path = "test_structure.dxf"
logger.info(f"Write layout: {output_path}")
layout.dbu = 0.001
layout.write(output_path)

